#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 13:56:51 2021

@author: kuriozity
"""

from functions import *
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
import numpy as np

if __name__ == '__main__':
    folder = r'/home/kuriozity/Bureau/program1/IRM_cut_plus'
    images = load_images_from_folder(folder)
    
    stain = []
    end_liver = 0
    i = 0 
    while end_liver == 0 and i < len(images):
            print('Process Image Number: ', i+1)
            end_liver,temp,images[i] = tempered_(images[i],1,0)
            stain.append(temp)
            i = i+1     
    
    section_max =  section_(stain,i)
    
    i = 0
    while end_liver == 0 and i < len(images):
            print('Process Image Number: ', i+1)
            end_liver,temp,images[i] = tempered_(images[i],0,section_max)
            stain.append(temp)
            i = i+1  
    
    stain = np.array(stain)
    contours_3d(stain,i)
